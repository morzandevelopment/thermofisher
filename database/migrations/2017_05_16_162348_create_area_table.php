<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAreaTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('area', function(Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('marketing_code');
            $table->string('slug');
            $table->string('value');
            $table->string('header_img');
            $table->string('background_img');
            $table->string('side_left');
            $table->string('side_right')->nullable();
            $table->string('main_color');
            $table->string('background_color')->nullable();
            $table->string('header_color')->nullable();
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::dropIfExists('area');
	}

}
