<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubscribersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('subscribers', function(Blueprint $table) {
        	$table->increments('id');
            $table->string('full_name');
            $table->string('cell_phone');
            $table->string('email')->unique();
            $table->string('company');
            $table->string('english_level')->nullable();
            $table->string('type');
            $table->boolean('candidate')->default(false);

            $table->integer('phoneAreas_id')->nullable()->unsigned()->index();
            $table->foreign('phoneAreas_id')->references('id')->on('phoneAreas')->onUpdate('cascade')->onDelete('cascade');
			$table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::dropIfExists('subscribers');
	}

}
