<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnExperienceCoordinatingOnPlantToSubscribers extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::table('subscribers', function(Blueprint $table)
        {
            $table->integer('experience_coordinate_maintenance_plant')->nullable();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::table('subscribers', function($table)
        {
            $table->integer('experience_coordinate_maintenance_plant')->nullable();
        });
	}

}
