<?php

use Illuminate\Database\Seeder;

class FiltersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('filters')->insert([
            'name' => 'Verde',
            'description' => 'Grupo de color verde',
            'color' => '#008000',
            'style' => 'width: 13px; height: 13px; border-radius: 25px;',
            'active' => 1
        ]);
        DB::table('filters')->insert([
            'name' => 'Azul',
            'description' => 'Grupo de color azul',
            'color' => '#0391ff',
            'style' => 'width: 13px; height: 13px; border-radius: 25px;',
            'active' => 1
        ]);
        DB::table('filters')->insert([
            'name' => 'Morado',
            'description' => 'Grupo de color amarillo',
            'color' => '#df37f9',
            'style' => 'width: 13px; height: 13px; border-radius: 25px;',
            'active' => 1
        ]);
        DB::table('filters')->insert([
            'name' => 'Rojo',
            'description' => 'Grupo de color rojo',
            'color' => '#f93737',
            'style' => 'width: 13px; height: 13px; border-radius: 25px;',
            'active' => 1
        ]);

    }
}
