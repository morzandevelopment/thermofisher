<?php

use Illuminate\Database\Seeder;

class AreasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('area')->insert([
            'name' => 'Developers (all levels)',
            'marketing_code' => "",
            'slug' => 'developers',
            'value' => 'developers',
            'header_img' => 'landing-page/header.jpg',
            'background_img' => 'landing-page/main-bg.png',
            'side_left' => 'landing-page/left-section-content.png',
            'main_color' => '#d21d24',
            'active' => 1
        ]);
        DB::table('area')->insert([
            'name' => 'Software Engineer, embedded',
            'marketing_code' => "",
            'slug' => 'software-engineer-embedded',
            'value' => 'software-engineer-embedded',
            'header_img' => 'landing-page/header.jpg',
            'background_img' => 'landing-page/main-bg.png',
            'side_left' => 'landing-page/left-section-content.png',
            'main_color' => '#d21d24',
            'active' => 1
        ]);
        
        DB::table('area')->insert([
            'name' => 'Software engineer, loT',
            'marketing_code' => "",
            'slug' => 'software-engineer-lot',
            'value' => 'software-engineer-lot',
            'header_img' => 'landing-page/header.jpg',
            'background_img' => 'landing-page/main-bg.png',
            'side_left' => 'landing-page/left-section-content.png',
            'main_color' => '#d21d24',
            'active' => 1
        ]);
        
        DB::table('area')->insert([
            'name' => 'Scrum Masters',
            'marketing_code' => "",
            'slug' => 'scrum-masters',
            'value' => 'scrum-masters',
            'header_img' => 'landing-page/header.jpg',
            'background_img' => 'landing-page/main-bg.png',
            'side_left' => 'landing-page/left-section-content.png',
            'main_color' => '#d21d24',
            'active' => 1
        ]);
        
        DB::table('area')->insert([
            'name' => 'Product Owners',
            'marketing_code' => "",
            'slug' => 'product-owners',
            'value' => 'product-owners',
            'header_img' => 'landing-page/header.jpg',
            'background_img' => 'landing-page/main-bg.png',
            'side_left' => 'landing-page/left-section-content.png',
            'main_color' => '#d21d24',
            'active' => 1
        ]);
        
        DB::table('area')->insert([
            'name' => 'UX Designers',
            'marketing_code' => "",
            'slug' => 'ux-designers',
            'value' => 'ux-designers',
            'header_img' => 'landing-page/header.jpg',
            'background_img' => 'landing-page/main-bg.png',
            'side_left' => 'landing-page/left-section-content.png',
            'main_color' => '#d21d24',
            'active' => 1
        ]);
        
        DB::table('area')->insert([
            'name' => 'Software Dev Manager',
            'marketing_code' => "",
            'slug' => 'software-dev-manager',
            'value' => 'software-dev-manager',
            'header_img' => 'landing-page/header.jpg',
            'background_img' => 'landing-page/main-bg.png',
            'side_left' => 'landing-page/left-section-content.png',
            'main_color' => '#d21d24',
            'active' => 1
        ]);
        
        DB::table('area')->insert([
            'name' => 'Quality Assurance Software Engineer',
            'marketing_code' => "",
            'slug' => 'quality-assurance-software-engineer',
            'value' => 'quality-assurance-software-engineer',
            'header_img' => 'landing-page/header.jpg',
            'background_img' => 'landing-page/main-bg.png',
            'side_left' => 'landing-page/left-section-content.png',
            'main_color' => '#d21d24',
            'active' => 1
        ]);

        DB::table('area')->insert([
            'name' => 'Staff Software Engineer',
            'marketing_code' => "",
            'slug' => 'staff-software-engineer',
            'value' => 'staff-software-engineer',
            'header_img' => 'landing-page/header.jpg',
            'background_img' => 'landing-page/main-bg.png',
            'side_left' => 'landing-page/left-section-content.png',
            'main_color' => '#d21d24',
            'active' => 1
        ]);

        DB::table('area')->insert([
            'name' => 'Java Developer (all levels)',
            'marketing_code' => "",
            'slug' => 'java-developer',
            'value' => 'java-developer',
            'header_img' => 'landing-page/header.jpg',
            'background_img' => 'landing-page/main-bg.png',
            'side_left' => 'landing-page/left-section-content.png',
            'main_color' => '#d21d24',
            'active' => 1
        ]);

        DB::table('area')->insert([
            'name' => '.Net developer (all levels)',
            'marketing_code' => "",
            'slug' => 'net-developer',
            'value' => 'net-developer',
            'header_img' => 'landing-page/header.jpg',
            'background_img' => 'landing-page/main-bg.png',
            'side_left' => 'landing-page/left-section-content.png',
            'main_color' => '#d21d24',
            'active' => 1
        ]);
        // DB::table('area')->insert([
        //     'name' => 'Gerente de Manufactura',
        //     'marketing_code' => "",
        //     'slug' => 'gerente-de-manufactura',
        //     'value' => 'gerente-de-manufactura',
        //     'header_img' => 'landing-page/header.jpg',
        //     'background_img' => 'landing-page/main-bg.png',
        //     'side_left' => 'landing-page/left-section-content.png',
        //     'main_color' => '#d21d24'
        // ]);

        // DB::table('area')->insert([
        //     'name' => 'Especialista en Control de Producción',
        //     'marketing_code' => "",
        //     'slug' => 'especialista-en-control-de-produccion',
        //     'value' => 'especialista-en-control-de-produccion',
        //     'header_img' => 'landing-page/header.jpg',
        //     'background_img' => 'landing-page/main-bg.png',
        //     'side_left' => 'landing-page/left-section-content.png',
        //     'main_color' => '#d21d24'
        // ]);
    }
}
