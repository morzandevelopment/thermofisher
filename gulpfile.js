var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Less
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
	// mix.less('app.less');
	mix
	.less('landing-page.less')
	.less('master.less')
	.copy('node_modules/jquery/dist/jquery.min.js', 'public/js/jquery.min.js')
	.copy('node_modules/jquery-validation/dist/jquery.validate.min.js', 'public/js/jquery.validate.min.js')
	.copy('node_modules/jquery-modal/jquery.modal.min.js', 'public/js/jquery.modal.min.js')
	.copy('node_modules/jquery-modal/jquery.modal.min.css', 'public/css/jquery.modal.min.css')
	.copy('resources/assets/js/switches.js', 'public/js/switches.js')
	.copy('resources/assets/js/jquery.maskedinput.min.js', 'public/js/jquery.maskedinput.min.js')
	.copy('resources/assets/js/vue.js', 'public/js/vue.js')
	.copy('resources/assets/js/vue-resource.js', 'public/js/vue-resource.js')
	.copy('resources/assets/js/dom-checkbox.js', 'public/js/dom-checkbox.js')
    .copy('resources/assets/js/parallax.min.js', 'public/js/parallax.min.js')
    .copy('resources/assets/js/jquery-ui-slider-pips.js', 'public/js/jquery-ui-slider-pips.js')
    .copy('resources/assets/css/jquery-ui-slider-pips.css', 'public/css/jquery-ui-slider-pips.css')

      // .copy('resources/assets/js/components/admin.js', 'public/js/components/admin.js')
	.scripts(['landing-page.js'], 'public/js/landing-page.js')
	.scripts(['components/admin.js'], 'public/js/components/admin.js');
});
