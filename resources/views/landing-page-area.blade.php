<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="_token" content=" {!! csrf_token() !!}" />

    <title>Solicitamos Asociados de Manufactura en Toyota</title>

    <link rel="stylesheet" type="text/css" href="{{ asset('css/jquery.modal.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/landing-page.css') }}">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
<div class="header">
    <div class="wrapper">
        <img class="header-image" src="{{ asset('images/'.$area->header_img.'') }}">
    </div>
</div>

<div class="main">
    <div class="wrapper">
        <div class="leftSection">
            <div class="inner">
                <img class="leftSection-image" src="{{ asset('images/'.$area->side_left) }}">
            </div>
        </div>

        <div class="rightSection">
            <div class="inner">
                {!! Form::open(['url' => url('').'/subscribe', 'method' => 'POST', 'id' => 'subscription-form', 'class' => 'subscription', 'files'=>true]) !!}
                <h2 class="subscription-title">
                    Déjanos tus datos y <b>nuestro personal</b>
                    se pondrá en contacto contigo.
                </h2>

                <label class="subscription-label subscription-selectLabel">
                    <h3 class="subscription-title"><center><strong>{{$area->name}}</strong></center></h3>
                </label>

                <div class="subscription-restOfForm ">
                    <label class="subscription-label">
                        Nombre completo
                        <input class="subscription-textbox" type="text" name="full_name" required id="full_name">
                    </label>

                    <label class="subscription-label">
                        Número de celular
                        <input class="subscription-textbox" type="tel" name="cell_phone" required id="cell_phone">
                    </label>

                    <label class="subscription-label">
                        Correo electrónico
                        <span class="helpBlock pull-right">opcional</span>
                        <input class="subscription-textbox" type="email" name="email" id="email">
                    </label>
                    <label class="subscription-label subscription-cvLabel">
                        Currículum vitae
                        <label class="subscription-fileLabel" for="cv" >
										<span class="" id="before-add-file">
											<b>Adjuntar en formato PDF</b>
											máximo de 2 MB
										</span>

                            <span class="hidden" id="after-add-file">
											<b>Tu curriculum vitae:</b>
												<span id="file-name"></span>
										</span>

                        </label>
                        <input class="subscription-fileInput" type="file" name="cv" id="cv" accept=".pdf" >
                        <div id="max_file_size" class="hidden ">
                            El tamaño maximo es de 2 Megabyte.
                        </div>
                    </label>


                    <label class="subscription-label subscription-agree">
                        <input class="subscription-checkbox" type="checkbox" name="agree" required>
                        <a class="subscription-agreeLink" href="{{ url('/our/terms-and-conditions') }}" rel="modal:open">
                            <span class="subscription-agreeText">Acepto los <b>términos y condiciones</b> de uso.</span>
                        </a>
                    </label>

                    <button class="subscription-submitButton" type="submit">
                        Enviar mis datos
                    </button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>

    <div class="footer">
        &copy; 2017 <a href="http://www.empleonuevo.com" target=_blank""><b>empleo</b>nuevo.com</a>
    </div>
</div>

<div id="end-applied-modal" class="modal" style="display:none">
</div>

<script type="text/javascript">
    /* <![CDATA[ */
    var google_conversion_id = 857615872;
    var google_custom_params = window.google_tag_params;
    var google_remarketing_only = true;
    /* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
    <div style="display:inline;">
        <img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/857615872/?guid=ON&amp;script=0"/>
    </div>
</noscript>

<script src="{{ asset('/js/jquery.min.js') }}"></script>
<script src="{{ asset('/js/jquery.validate.min.js') }}"></script>
<script src="{{ asset('/js/jquery.modal.min.js') }}"></script>
<script src="{{ asset('/js/jquery.maskedinput.min.js') }}"></script>
<script src="{{ asset('/js/jquery.form.js') }}"></script>
<script src="{{ asset('/js/helper.js') }}"></script>
<script src="{{ asset('/js/landing-page.js') }}"></script>

<input type="hidden" id="root-url" value="{{url('')}}">
</body>
</html>