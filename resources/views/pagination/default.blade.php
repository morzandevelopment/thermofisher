@if (isset($paginator) && $paginator->lastPage() > 1)

	<ul class="pagination">
		<li class="pagination-item">
			Ir a página
		</li>

	<?php
	$interval = isset($interval) ? abs(intval($interval)) : 3 ;
	$from = $paginator->currentPage() - $interval;
	if($from < 1){
		$from = 1;
	}

	$to = $paginator->currentPage() + $interval;
	if($to > $paginator->lastPage()){
		$to = $paginator->lastPage();
	}
	?>

	<!-- first/previous -->
		@if($paginator->currentPage() > 1)


			<li class="pagination-item">
				<a class="pagination-button" href="{{ str_replace('/?',"?", $subscribers->url(($subscribers->currentPage()-1))) }}" aria-label="Previous">
					Anterior
				</a>
			</li>
		@endif

	<!-- links -->
		@for($i = $from; $i <= $to; $i++)
			<?php
			$isCurrentPage = $paginator->currentPage() == $i;
			?>
			<li class="pagination-item {{ $isCurrentPage ? 'active' : '' }}">
				<a class="pagination-button" href="{{ !$isCurrentPage ?str_replace('/?',"?", $subscribers->url($i)) : '#' }}">
					{{ $i }}
				</a>
			</li>
		@endfor

	<!-- next/last -->
		@if($paginator->currentPage() < $paginator->lastPage())
			<li class="pagination-item">
				<a class="pagination-button" href="{{ str_replace('/?',"?", $subscribers->url(($subscribers->currentPage()+1))) }}" aria-label="Next">
					Siguiente
				</a>
			</li>

		@endif

	</ul>

@endif