@if (isset($paginator) && $paginator->lastPage() > 1)

    <ul class="pagination">
    <?php
    $interval = isset($interval) ? abs(intval($interval)) : 3 ;
    $from = $paginator->currentPage() - $interval;
    if($from < 1) {
      $from = 1;
    }

    $to = $paginator->currentPage() + $interval;
    if($to > $paginator->lastPage()) {
      $to = $paginator->lastPage();
    }
    ?>

    <!-- first/previous -->
         {{--@if($paginator->currentPage() > 1)--}}
        <li class="pagination-item">
             <a class="pagination-button {{ $paginator->currentPage() > 1 ? '' : 'pagination-disabledButton' }}" href="{{ '?' . http_build_query(array_merge($_GET, array('page' => $subscribers->currentPage() - 1))) }}" aria-label="Previous">
                <i class="fa fa-chevron-left" aria-hidden="true"></i>
            </a>
        </li>
         {{--@endif--}}


    <!-- next/last -->
         {{--@if($paginator->currentPage() < $paginator->lastPage())--}}
        <li class="pagination-item">
             <a class="pagination-button {{ $paginator->currentPage() < $paginator->lastPage() ? '' : 'pagination-disabledButton' }}" href="{{ '?' . http_build_query(array_merge($_GET, array('page' => $subscribers->currentPage() + 1))) }}" aria-label="Next">
                <i class="fa fa-chevron-right" aria-hidden="true"></i>
            </a>
        </li>
         {{--@endif--}}
    </ul>

@else

    <ul class="pagination">
    <?php
    $interval = isset($interval) ? abs(intval($interval)) : 3 ;
    $from = $paginator->currentPage() - $interval;
    if($from < 1) {
        $from = 1;
    }

    $to = $paginator->currentPage() + $interval;
    if($to > $paginator->lastPage()) {
        $to = $paginator->lastPage();
    }
    ?>

    <!-- first/previous -->
        {{--@if($paginator->currentPage() > 1)--}}
        <li class="pagination-item">
            <a class="pagination-button {{ $paginator->currentPage() > 1 ? '' : 'pagination-disabledButton' }}" href="{{ '?' . http_build_query(array_merge($_GET, array('page' => $subscribers->currentPage() - 1))) }}" aria-label="Previous">
                <i class="fa fa-chevron-left" aria-hidden="true"></i>
            </a>
        </li>
        {{--@endif--}}


    <!-- next/last -->
        {{--@if($paginator->currentPage() < $paginator->lastPage())--}}
        <li class="pagination-item">
            <a class="pagination-button {{ $paginator->currentPage() < $paginator->lastPage() ? '' : 'pagination-disabledButton' }}" href="{{ '?' . http_build_query(array_merge($_GET, array('page' => $subscribers->currentPage() + 1))) }}" aria-label="Next">
                <i class="fa fa-chevron-right" aria-hidden="true"></i>
            </a>
        </li>
        {{--@endif--}}
    </ul>

@endif