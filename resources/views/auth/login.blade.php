@extends('layouts.master')

@section('content')

	<div class="login">
		<div class="wrapper">
			<h2 class="title text-center">
				¡Inicia <b>sesión</b>!
			</h2>

			<p class="subtitle text-center">
				Ingresa tu acceso asignado para comenzar
			</p>

			@if (count($errors))
				@foreach($errors->all() as $error)
					<div class="alert-error text-center">
						Verifique la información ingresada.
					</div>
				@endforeach
			@endif

			<form class="login-form" role="form" method="POST" action="{{ url('/auth/login') }}">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">

				<label class="form-label">
					Correo electrónico
					<input type="email" class="form-textbox expanded" name="email" value="{{ old('email') }}" autofocus>
				</label>

				<label class="form-label">
					Contraseña
					<input type="password" class="form-textbox expanded" name="password">
				</label>

				<label class="form-label">
					<input class="form-rememberCheckbox" type="checkbox" name="remember"> 
					Recuérdame
				</label>

				<button type="submit" class="form-submitButton expanded">
					Iniciar sesión
				</button>
			</form>
		</div>
	</div>

@endsection
