<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta name="_token" content=" {!! csrf_token() !!}" />

		<title>ThermoFisher</title>

        <link rel="stylesheet" type="text/css" href="{{ asset('css/jquery.modal.min.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('css/jquery-ui-slider-pips.css') }}">
        <link rel="stylesheet" href="https://code.jquery.com/ui/1.10.4/themes/flick/jquery-ui.css">
        <link rel="stylesheet" type="text/css" href="{{ asset('css/landing-page.css') }}">

		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>

	<body>

		<div class="header-facilities" id="main-header">
			{{--<div class="wrapper" id="header-image-facilities">--}}
                {{--<img class="header-image" src="{{ asset("images/landing-page/header-facilities-and-maintenance-manager.png") }}">--}}
			{{--</div>--}}
            <div class="wrapper" id="header-image">
                <img class="header-image" src="{{ asset("images/landing-page/header.jpg") }}">
            </div>
		</div>

		<div class="main-facilities" id="main-bg">
			<div class="wrapper">
				<div class="leftSection">
					<div class="inner">
						<img class="leftSection-image" src="{{ asset('images/landing-page/left-section-content.png') }}">
                    </div>
					<div class="inner">
						<div class="leftSection-video ">
							<iframe width="410" height="275" src="https://www.youtube.com/embed/bwaRCqUytBk?autoplay=1" frameborder="0" allowfullscreen></iframe>
						</div>
					</div>
				</div>

				<div class="rightSection">
					<div class="inner">
						{!! Form::open(['url' => url('').'/subscribe', 'method' => 'POST', 'id' => 'subscription-form', 'class' => 'subscription', 'files'=>true]) !!}
							<h2 class="subscription-title">
								Share your information and we will contact you as soon as possible:
							</h2>

							<label class="subscription-label subscription-selectLabel">
								Available positions:
								<select class="subscription-select" name="type">
                                    @foreach($areas as $interes)
                                        <option value="{{$interes->value}}" {{ ( isset($area) ? ($area == $interes->slug) ? 'selected' : '' : '')}}>{{$interes->name}}</option>
                                    @endforeach
								</select>
							</label>

							<div class="subscription-restOfForm">
								<label class="subscription-label">
									Name:
									<input class="subscription-textbox" type="text" name="full_name" required id="full_name">
								</label>

								<label class="subscription-label">
									Cellphone:
									<input class="subscription-textbox" type="tel" name="cell_phone" required id="cell_phone">
								</label>

								<label class="subscription-label">
									Email:
									{{--<span class="helpBlock pull-right">opcional</span>--}}
									<input class="subscription-textbox" type="email" name="email" id="email">
								</label>

                                <label class="subscription-label">
                                    English level:
                                    {{--<span class="helpBlock pull-right">slide sideways to choose your percentage</span><br>--}}
                                     {{--<input type="range" min="1" max="100" step="1" id="english_level" name="english_level">--}}
                                    <div class="subscription-slider"></div>
                                    <input type="number" name="english_level" id="english_level" class="subscription-englishLevel" required>
                                </label>

								

                                {{--<label class="subscription-label">--}}
                                    {{--Empresa--}}
                                    {{--<span class="helpBlock pull-right">opcional</span>--}}
                                    {{--<input class="subscription-textbox" type="text" name="company" id="company">--}}
                                {{--</label>--}}

                                {{--<label class="subscription-label">--}}
                                    {{--<span id="years_experience-label">Años de experiencia en industria médica</span>--}}
                                    {{--<input class="subscription-textbox-small" type="number" name="years_experience" id="years_experience">--}}
                                {{--</label>--}}

								<label class="subscription-label subscription-cvLabel">
									Share your resume
                                    {{--<span class="helpBlock pull-right">opcional</span>--}}
									<label class="subscription-fileLabel" for="cv" >
										<span class="" id="before-add-file">
											<b>PDF format only</b>
											with 2MB of size maximum
										</span>

										<span class="hidden" id="after-add-file">
											<b>Your resume:</b>
												<span id="file-name"></span>
										</span>

									</label>
									<input class="subscription-fileInput" type="file" name="cv" id="cv" accept=".pdf" >
									<div id="max_file_size" class="hidden ">
										The max size is 2MB.
									</div>
								</label>
								
								<label class="subscription-label subscription-agree">
									<input class="subscription-checkbox" type="checkbox" name="agree" required>
									<a class="subscription-agreeLink" href="{{ url('/our/terms-and-conditions') }}" rel="modal:open">
										<span class="subscription-agreeText">I accept the <b>terms and conditions</b> of use.</span>
									</a>
								</label>

								<button class="subscription-submitButton" type="submit">
									Send my information
								</button>
							</div>
						{!! Form::close() !!}
					</div>
				</div>
			</div>

			<div class="footer">
				&copy; {{ date('Y') }} <a href="http://www.empleonuevo.com" target=_blank""><b>empleo</b>nuevo.com</a>
			</div>
		</div>

		<div id="end-applied-modal" class="modal" style="display:none">
		</div>

		<!-- Google Code para etiquetas de remarketing -->
		<!--------------------------------------------------
		Es posible que las etiquetas de remarketing todavía no estén asociadas a la información personal identificable o que estén en páginas relacionadas con las categorías delicadas. Para obtener más información e instrucciones sobre cómo configurar la etiqueta, consulte http://google.com/ads/remarketingsetup.
		--------------------------------------------------->
		<script type="text/javascript">
		/* <![CDATA[ */
		var google_conversion_id = 799649577;
		var google_custom_params = window.google_tag_params;
		var google_remarketing_only = true;
		/* ]]> */
		</script>
		<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
		</script>
		<noscript>
		<div style="display:inline;">
		<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/799649577/?guid=ON&amp;script=0"/>
		</div>
		</noscript>

        <script src="{{ asset('/js/jquery.min.js') }}"></script>
        <script src="{{ asset('/js/jquery.validate.min.js') }}"></script>
        <script src="{{ asset('/js/jquery.modal.min.js') }}"></script>
        <script src="{{ asset('/js/jquery.maskedinput.min.js') }}"></script>
        <script src="{{ asset('/js/jquery.form.js') }}"></script>
        <script src="{{ asset('/js/helper.js') }}"></script>
        <script src="https://code.jquery.com/ui/1.11.1/jquery-ui.js"></script>
        <script src="{{ asset('/js/jquery-ui-slider-pips.js') }}"></script>
        <script src="{{ asset('/js/landing-page.js') }}"></script>

		<input type="hidden" id="root-url" value="{{url('')}}">
	</body>
</html>