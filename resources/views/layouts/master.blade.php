<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="csrf-token" content="{{ csrf_token() }}">
		<meta name="_token" content=" {!! csrf_token() !!}" />

		<title>ThermoFisher</title>

		<link rel="stylesheet" href="{{ asset('/css/jquery.modal.min.css') }}" type="text/css" media="screen" />
		{{-- <link rel="stylesheet" type="text/css" href="http://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css"> --}}
		<link rel="stylesheet" type="text/css" href="{{ asset('css/master.css') }}">

		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->

        <link rel="stylesheet" href="https://cdn.jsdelivr.net/jquery.webui-popover/1.2.1/jquery.webui-popover.min.css">
	</head>

	<body>
		<header class="header">
			<div class="wrapper">
				<img class="header-logo" src="{{ asset('images/header-logo.png') }}">
				@if (Auth::user())
					<a class="header-logoutLink pull-right" href="{{  url('/admin/salir') }}">Cerrar sesión</a>
				@else
					@endif
			</div>
		</header>

		<main class="content">
			@yield('content')
		</main>

		<footer class="footer">
			&copy; {{ date('Y') }} <a href="http://www.empleonuevo.com" target=_blank""><b>empleo</b>nuevo.com</a>
		</footer>


        <script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

            ga('create', 'UA-64023000-14', 'auto');
            ga('send', 'pageview');

        </script>

		<script src="{{ asset('/js/jquery.min.js') }}"></script>
		<script src="{{ asset('/js/vue.js') }}"></script>
		<script src="{{ asset('/js/vue-resource.js') }}"></script>
        <script src="https://cdn.jsdelivr.net/jquery/1.11.3/jquery.min.js"></script>
        <script src="https://cdn.jsdelivr.net/jquery.webui-popover/1.2.1/jquery.webui-popover.min.js"></script>

		@yield('javascript')

		<input type="hidden" id="root-url" value="{{url('')}}">
	</body>
</html>