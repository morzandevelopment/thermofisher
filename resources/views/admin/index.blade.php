@extends('layouts.master')

@section('styles')
    <style>
        .swal2-content p {
            margin: 0px auto 15px;
        }
        .dateDisplay {
            background-color: #FEFAE3;
            padding: 17px;
            border: 1px solid #F0E1A1;
            display: block;
            margin: 10px 22px 22px;
            text-align: center;
            color: #61534e;
        }
        .swal2-actions {
            margin: 0px!important;
        }
        .confirmSwalButton {
            width: 100%;
            display: block;
        }
    </style>
@endsection

@section('content')
	<script type="text/javascript">
		function updateQueryStringParameter(uri, key, value) {
			var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
			var separator = uri.indexOf('?') !== -1 ? "&" : "?";
			if (uri.match(re))
				return uri.replace(re, '$1' + key + "=" + value + '$2');
			else
				return uri + separator + key + "=" + value;
		}
	</script>
    @if (isset($notify_limit_date))
        <input type="text" name="notify_limit_date" hidden value="{{$notify_limit_date['response'] == true ? $notify_limit_date["date"] : '0'}}">
    @endif
	<input type="hidden" id="root-url" value="{{ url('') }}">
    <meta name="csrf-token" content="{{ csrf_token() }}">

	{{-- <input type="hidden" name="sort_by" value="">
	<input type="hidden" name="direction" value=""> --}}

	<div class="admin" id="adminVue">
		<div id="comment-modal" class="modal" style="display: none;">
			<div class="modal-header">
				<h2 class="modal-title">
					Agregar <b>comentario</b>
				</h2>
			</div>

			<div class="modal-body">
				<textarea class="modal-textarea" :value="comment" v-model="comment" placeholder="Escribe tu comentario..."></textarea>
			</div>

			<div class="modal-footer">
				<button class="modal-button" v-on:click="saveComment()">
					Guardar comentario
				</button>

				<button class="modal-button-close">
					Cancelar
				</button>
			</div>
		</div>

		<div class="stage parallax-window" data-parallax="scroll" data-image-src="{{ asset('images/admin-header.jpg') }}">
		{{-- <div class="stage parallax-window"> --}}
			<div class="wrapper">
				<h2 class="title">
					¡Bienvenid@ a la <b>base de datos</b>!
				</h2>

				<p class="subtitle">
					Información de <b>{{ $total }} aspirante{{ ($subscribers->total() > 1 || $subscribers->total() == 0  ) ? 's' : '' }} </b> registrad{{ ($subscribers->total() > 1 || $subscribers->total() == 0  ) ? 'os' : 'o' }} a la campaña.
				</p>
			</div>
		</div>

        <div class="tableBar tableBar-top">
				<div class="wrapper">
					<div class="tableBar-left">
                        {!! Form::open(array('route' => 'export-excel')) !!}
						<select class="tableBar-select" onchange="if (this.value) window.location.href = '{{ route('admin.index') }}' + '?type=' + this.value" name="type">
                            <option value="all">Todos</option>
                            @foreach ($areas as $area)
                                <option value="{{$area->slug}}" {{($type == $area->slug) ? 'selected' : ''}}>{{$area->name}}</option>
                            @endforeach
                        </select>
                        <button type="submit" class="tableBar-exportButton" :disabled="( {{$subscribers->count()}} === 0) ? true : false">
                            Descargar candidatos
                        </button>
                        {!! Form::close() !!}
					</div>

                    @if($notify_limit_date['response'] == true)
					<div class="tableBar-right">
                        {!! Form::open(array('route' => 'download-all')) !!}
                            <button class="tableBar-downloadAllButton" :disabled="( {{$subscribers->count()}} === 0) ? true : false">
                                Descargar candidatos y cvs
                            </button>
                        {!! Form::close() !!}
					</div>
                    @else
                    @endif
				</div>
			</div>


			<div class="wrapper">
				<table class="table">
					<thead>
						<tr>
                            <th>
                                #
                            </th>
                            <th>
                                <a href="{!! sortSubscribersBy('created_at') !!}">
                                    <i class="fa fa-sort" aria-hidden="true"></i>
                                </a>
                                Fecha
                            </th>
                            <th>
                                <a href="{!! sortSubscribersBy('full_name') !!}">
                                    <i class="fa fa-sort" aria-hidden="true"></i>
                                </a>
                                Nombre completo
                            </th>
                            <th>
                                <a href="{!! sortSubscribersBy('cell_phone') !!}">
                                    <i class="fa fa-sort" aria-hidden="true"></i>
                                </a>
                                Teléfono
                            </th>
                            <th>
                                <a href="{!! sortSubscribersBy('email') !!}">
                                    <i class="fa fa-sort" aria-hidden="true"></i>
                                </a>
                                Correo electrónico
                            </th>
                            <th>
                                <a href="{!! sortSubscribersBy('type') !!}">
                                    <i class="fa fa-sort" aria-hidden="true"></i>
                                </a>
                                Vacante de interés
                            </th>
                            <th>
                                <a href="{!! sortSubscribersBy('english_level') !!}">
                                    <i class="fa fa-sort" aria-hidden="true"></i>
                                </a>
                                Inglés
                            </th>
                            <th class="table-title-applicant">
                                <a href="{!! sortSubscribersBy('candidate') !!}">
                                    <i class="fa fa-sort" aria-hidden="true"></i>
                                </a>
                                Candidato
                            </th>
                            <th>
                                <a href="{!! sortSubscribersBy('cv') !!}">
                                    <i class="fa fa-sort" aria-hidden="true"></i>
                                </a>
                                CV
                            </th>
						</tr>
					</thead>

                    <tbody>
                    <?php $cont = (($subscribers->currentPage()-1) * $show); ?>
                    @if($subscribers->count() >= 1)
                        @foreach($subscribers as $key=>$subscribe)
                            <tr>
                                <td>
                                    {{  $cont+=1 }}
                                </td>
                                <td>
                                    <?php
                                    setlocale(LC_TIME, 'es_ES.UTF-8');
                                    date_default_timezone_set('America/Tijuana');
                                    $date = strftime("%d %h", strtotime($subscribe->created_at));
                                    $dateTooltip = strftime("%A, %d de %B de %Y a las %l:%M", strtotime($subscribe->created_at)) . ' '.(strftime("%H:%M", strtotime($subscribe->created_at)) >= 12 ? 'PM' : 'AM');
                                    ?>
                                    <div id="date-tooltip" class="has-tip" rel="tooltip">{{ $date }}
                                        <span id="tooltip-text">{{ucfirst($dateTooltip)}}</span>
                                    </div>
                                </td>
                                <td>
                                    {{--<div style="width:100%; display: inline-flex;">--}}
                                    {{--<div style="width: 6%; margin-top: 5px;">--}}
                                    {{--<div id="color-{{$subscribe->id}}" class="tag-color" value="{{$subscribe->id}}" style="width:13px;height:13px;-webkit-border-radius: 25px;-moz-border-radius: 25px;border-radius: 25px;background: {{(!empty($subscribe->filter()->color) ? $subscribe->filter()->color : '#f5f5f5')}}"></div>--}}
                                    {{--</div>--}}
                                    {{--<div style="width: 94%;">--}}
                                    {{--{{  $subscribe->full_name }}--}}
                                    {{--</div>--}}
                                    {{--<a class="comment-link" href="#"  title="Comentar" v-on:click="getComment('{{  $subscribe->id }}', $event)">--}}
                                    {{--@if ($subscribe->comment)--}}
                                    {{--@if ($subscribe->comment->comment == '')--}}
                                    {{--<i class="fa fa-comment-o" aria-hidden="true" id="comment-{{$subscribe->id}}"></i>--}}
                                    {{--@else--}}
                                    {{--<i class="fa fa-comment" aria-hidden="true" id="comment-{{$subscribe->id}}"></i>--}}
                                    {{--@endif--}}
                                    {{--@else--}}
                                    {{--<i class="fa fa-comment-o" aria-hidden="true" id="comment-{{$subscribe->id}}"></i>--}}
                                    {{--@endif--}}
                                    {{--</a>--}}
                                    {{--</div>--}}
                                    {{  $subscribe->full_name }}
                                    <a class="comment-link" href="#"  title="Comentar" v-on:click="getComment('{{  $subscribe->id }}', $event)">
                                        @if ($subscribe->comment)
                                            @if ($subscribe->comment->comment == '')
                                                <i class="fa fa-comment-o" aria-hidden="true" id="comment-{{$subscribe->id}}"></i>
                                            @else
                                                <i class="fa fa-comment" aria-hidden="true" id="comment-{{$subscribe->id}}"></i>
                                            @endif
                                        @else
                                            <i class="fa fa-comment-o" aria-hidden="true" id="comment-{{$subscribe->id}}"></i>
                                        @endif
                                    </a>
                                </td>
                                <td>
                                    {{  $subscribe->cell_phone }}
                                </td>
                                <td>
                                    {{  $subscribe->email }}
                                </td>
                                <td>
                                    {{  $subscribe->type }}
                                </td>
                                <td class="text-center">
                                    {{ (!empty($subscribe->english_level) ? $subscribe->english_level.'%' : 'Sin especificar' ) }}
                                </td>
                                <td>
                                    <div class="switch {{ ( $subscribe->candidate == '1') ? 'active' : ''   }}" v-on:click="toggleCandidate('{{  $subscribe->id }}')">
                                        <span> {{ ( $subscribe->candidate == '1') ? 'si' : 'no'   }}</span>
                                        <input type="checkbox" name="candidate"  {{ ( $subscribe->candidate == '1') ? 'checked' : ''   }}>
                                    </div>
                                </td>
                                <td>
                                    @if($subscribe->file)
                                        <a target="_blank" href="{{url('')}}/uploads/cvs/subscribers_{{$subscribe->id}}/{{($subscribe->file != null) ? $subscribe->file->fileurl : ''}}">Ver CV</a>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="8" class="text-center">Aún no hay registros</td>
                        </tr>
                    @endif
                    </tbody>
				</table>

				<input type="hidden" value="20" name="contador">

				<div class="tableBar tableBar-bottom">
					<div class="tableBar-left">
						{{-- Mostrando --}}

						{!! Form::open(['route' => ['admin.index'], 'method' => 'get']) !!}
							Mostrando
							<select class="tableBar-select" onchange="if (this.value)  window.location.href = updateQueryStringParameter(window.location.href, 'rowsByPage', this.value)" name="number">
								<option value="20" {{ ($show == '20') ? 'selected' : '' }}>20</option>
								<option value="50" {{ ($show == '50') ? 'selected' : '' }}>50</option>
								<option value="100" {{ ($show == '100') ? 'selected' : '' }}>100</option>
								<option value="500" {{ ($show == '500') ? 'selected' : '' }}>500</option>
							</select>
	{{--						Mostrando registrados del {{ (($subscribers->currentPage()-1) * 4) + $subscribers->count() }}-{{ (($subscribers->currentPage()) * 4) + $subscribers->count()  }} de {{ $subscribers->total() }}--}}
							del {{ (($subscribers->currentPage()-1) * $show) + 1  }} al {{ $cont }} de {{ $subscribers->total() }} registrados.
						{!! Form::close() !!}
					</div>

					<div class="tableBar-right">
						@include('pagination.custom', ['paginator' => $subscribers, 'interval' => 20])
					</div>
				</div>
			</div>
		</div>
	</div>

    @section('javascript')
		<script src="{{ asset('/js/switches.js') }}"></script>
		<script src="{{ asset('/js/vue.js') }}"></script>
		<script src="{{ asset('/js/vue-resource.js') }}"></script>
		<script src="{{ asset('/js/components/admin.js') }}"></script>

		<script src="https://use.fontawesome.com/e8d0231282.js"></script>
		<script src="{{ asset('/js/jquery.modal.min.js') }}" type="text/javascript" charset="utf-8"></script>
		<script src="{{ asset('/js/parallax.min.js') }}"></script>

        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@7.26.11/dist/sweetalert2.all.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js"></script>
		<script type="text/javascript">
			$(function() {
                $('body').on('click', '.switch', function() {
					if ($(this).hasClass('active')) {
						$(this).children('span').text('si');
						$(this).find('input').prop('checked', true);
					} else {
						$(this).children('span').text('no');
						$(this).find('input').prop('checked', false);
					}
				});

				$('.modal-button-close').click(function(event) {
					$.modal.close();
				});

                var tagColor = $('.tag-color');

				tagColor.on('click', function () {

                    var subscriberID = $(this).attr('value');

                    $('#color-'+subscriberID).webuiPopover({
                        title:'Agregar a etiqueta',
                        type:'async',
                        url: 'http://'+window.location.host+'/admin/get-filters',
                        content:function(data) {
                            var html = '';
                            for (var key in data) {
                                html += '<div id="' + data[key].id + '" onclick="addFilter(this.id)" style="display: inline-flex; width:13px;height:13px;-webkit-border-radius: 25px;-moz-border-radius: 25px;border-radius: 25px;background:' + data[key].color + '; margin-bottom: 15px"><span style="padding-left: 25px">' + data[key].name + ' </span> <i class="fa fa-check" aria-hidden="true" id="' + data[key].id + '"></i></div><br>';
                            }
                            return html;
                        },
                        placement:'bottom',
                        trigger:'click',
                        width:190,
                        height:130,
                        animation:'pop',
                        multi:false,
                        padding:true,
                        dismissible:true
                    });
                   // WebuiPopovers.updateContentAsync({'filterID' : filterID, 'subcriberID' : subscriberID},'http://'+window.location.host+'/admin/save-filter');

                });
			});
           function addFilter(filterID)
            {
                var subscriberID = $('.tag-color').attr('value');
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                    url: 'http://'+window.location.host+'/admin/save-filter',
                    method : 'POST',
                    data: {'filterID' : filterID, 'subcriberID' : subscriberID},
                    success: function(result) {
                        console.log(result);
                    }
                });
            }

            //Manages red date notification
            if ($('input[name="notify_limit_date"]').val() != 0) {
                // $.removeCookie('red_notif')
                console.log('has notification cookie: '+$.cookie('red_notif'))
                
                    let date = $('input[name="notify_limit_date"]').val();
                    swal({
                        title: '¡Atención!',
                        html:
                            "<p>La fecha límite de este administrador está acercandose.</p>"+
                            "<p>Te recomendamos presionar el boton de <b>Descargar candidatos</b> pronto.</p>"+
                            "<span>Disponible hasta: <small>(dd-mm-aaaa)</small></span>"+
                            "<div class='dateDisplay'>"+date+"</div>",
                        type: 'info',
                        confirmButtonClass: "confirmSwalButton",
                        cancelButtonText: 'Descargar después',
                        confirmButtonColor: '#0f6ac9',
                        confirmButtonText: 'De acuerdo',
                    })
                    $.cookie("red_notif", true, { expires : 2 });
            }
		</script>
	@endsection
@endsection


