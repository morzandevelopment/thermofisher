$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
    }
});

var subscription_agreeText = '<span class="subscription-agreeText">Acepto los <b>términos y condiciones</b> de uso.</span>';


function form_submit (form) {

    var button  = $(form).find('input[type=submit]'),
        text    = button.val(),
        type    = $(form).find('[name=_method]').val();

    if ( !type) type  = $(form).attr('method');


    $(form).ajaxSubmit({
        beforeSend: function() {

            $('.subscription-submitButton').prop('disabled', true).text('Enviando...');
        },
        error: function(response)
        {

            var errors = (response.responseText) ? $.parseJSON(response.responseText) : "";

            var errors_html = "";

            $.each(errors, function(index, value) {
                errors_html += value + '<br />';
            });

            // if (errors) alertBoxButton('Error', errors_html, "error", true, true);
            //
            // alertBoxConnectionError();

        },
        beforeSend: function() {
            $('.subscription-submitButton').prop('disabled', true).text('Enviando...');
        },
        success: function(response)
        {
            $('.subscription-submitButton').text('En breve te contactaremos').addClass('subscription-successfulButton');
            $('.subscription').addClass('subscription-successful');

            // Ing.ABEL RUIZ
            // abre el modal al finazilar la postulacion.
            $('#end-applied-modal')
                .load($('#root-url').val()+'/subscribe/get-content-end-applied')
                .modal('open');

            //limpio el contenido del modal.
            $('#end-applied-modal').on($.modal.BEFORE_CLOSE, function(event, modal) {
                $(this).html('');
            });
        },
        complete: function() {
            return button.val(text).prop('disabled', false);
        },
        error: function(data) {
            // console.log(data);
            $('.subscription-submitButton').prop('disabled', false).html(subscription_agreeText);
        },
        dataType: 'json',
        type: type
    });
}