$(function() {
	$('.subscription-select').change(function() {
	    alert('entro');
		if ($(this).val() == 'professional') {
			$('.subscription-fileInput').prop('required', true);
			$('.subscription-restOfForm').removeClass('forProduction').addClass('forProfessional').slideDown();
		} else if ($(this).val() == 'production') {
			$('.subscription-fileInput').prop('required', false);
			$('.subscription-restOfForm').removeClass('forProfessional').addClass('forProduction').slideDown();
		}

		validator.resetForm();

		$('.subscription-checkbox').removeClass('error').attr('aria-invalid', 'false').closest('label').removeClass('error');
		$('.subscription-agreeLink').removeClass('error').html(subscription_agreeText);
	});

	var token = $('meta[name="csrf-token"]').attr('content');

	var validator = $('#subscription-form').validate({
		rules: {
			full_name: {
				required: true,
				minlength: 3
			},
			cell_phone: {
				required: true
			},
			email: {
				// required: true,
				email: true,
				remote: {
					url: $('#root-url').val()+'/validate-email',
					type: 'post',
					data: {
						_token: function() {
							return token
						}
					}
				}
			},
			agree: {
				required: true
			}
		},
		messages: {
			full_name: {
				required: 'Por favor escribe tu nombre',
				minlength: 'Por favor escribe tu nombre'
			},
			cell_phone: {
				required: 'Por favor escribe tu número de celular'
			},
			email: {
				// required: 'Por favor escribe tu correo electrónico',
				email: 'Escribe un correo electrónico válido',
				remote: 'Ya se registraron con este correo electrónico'
			},
			agree: {
				required: 'Favor de aceptar los <b>términos y condiciones</b> de uso.'
			}
		},
		errorElement: 'span',
		errorPlacement: function(error, element) {
			// console.log(error);

			if (element.attr('name') == 'agree') {
                $('.subscription-agreeText').html($(error).text()).closest('label').addClass('error');
            } else {
            	error.insertAfter(element);
            }
		},
		submitHandler: submitForm
	});

	var subscription_agreeText = '<span class="subscription-agreeText">Acepto los <b>términos y condiciones</b> de uso.</span>';

	function submitForm() {
		var data = $('#subscription-form').serialize();

		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': token
			}
		});

		
		$.ajax({
			type: 'post',
			url: $('#root-url').val()+'/subscribe',
			data: data,
			beforeSend: function() {
				$('.subscription-submitButton').prop('disabled', true).text('Enviando...');
			},
			success: function(data) {
				console.log(data);
				$('.subscription-submitButton').text('En breve te contactaremos').addClass('subscription-successfulButton');
				$('.subscription').addClass('subscription-successful');
			},
			error: function(data) {
				// console.log(data);
				$('.subscription-submitButton').prop('disabled', false).html(subscription_agreeText);
			}
		});

		return false;
	};

	$('.subscription-checkbox').click(function() {
		if ($(this).is(':checked'))
			$('.subscription-agreeText').html(subscription_agreeText).closest('label').removeClass('error').attr('aria-invalid', 'true');
	});

	$('body').on('click', '.termsAndConditions-acceptButton', function() {
		$('.subscription-checkbox').prop('checked', true).removeClass('error').attr('aria-invalid', 'false').closest('label').removeClass('error');
		$('.subscription-agreeLink').removeClass('error').html(subscription_agreeText);
	});

	$('.subscription-textbox[type="tel"]').mask('(999)999-9999');
});
