/**
 * Created by obelich on 19/03/17.
 */

Vue.http.headers.common['X-CSRF-TOKEN'] = $('meta[name=_token]').attr('content');


var register = new Vue({
	el: '#adminVue',

	data: {
		test: "Hola VueJS",
		commentiId:  null,
		subscriberId:  null,
		comment: ''
	},

	methods: {
		toggleCandidate: function(id){

			this.$http.get($('#root-url').val()+'/setcandidate/'+id).then(function(response) {
				this.someData = response.body;

				// get body data

			}, function(error) {
				// error callback
			})
		},

		getComment: function (id, $event) {
			$event.preventDefault();

			this.subscriberId = id;
			_this = this;
			this.$http.get($('#root-url').val()+'/admin/comments/'+id).then(function(response) {
				if (response.body.success)
				{
					_this.comment = response.body.success.comment;
					_this.commentiId = response.body.success.id;
				}
				else
				{
					_this.comment = '';
					_this.commentiId = null;
				}
				$('#comment-modal').modal('open');
				// this.someData = response.body;

				// get body data

			}, function(error) {
				// error callback
			});
		},

		saveComment: function(){
			_this = this;
			if(this.commentiId === null)
			{
				this.save();
			}
			else
			{
				this.update();
			}


		},


		save: function(){
			this.$http.post($('#root-url').val()+'/admin/comments', {subscriber_id: this.subscriberId, comment: this.comment}).then(function(response) {
				var icon = $('#comment-'+this.subscriberId);
				console.log(response);
				icon.removeClass('fa fa-comment-o');
				icon.addClass('fa fa-comment');
				$.modal.close();
				// this.someData = response.body;

				// get body data

			}, function(error) {
				// error callback
			});

		},

		update: function(){

			this.$http.put($('#root-url').val()+'/admin/comments/'+this.commentiId, {id: this.commentiId, subscriber_id: this.subscriberId, comment: this.comment}).then(function(response) {
				var icon = $('#comment-'+this.subscriberId);
				if (this.comment == '')
				{

					icon.removeClass('fa fa-comment');
					icon.addClass('fa fa-comment-o');
				}
				else
				{
					icon.removeClass('fa fa-comment-o');
					icon.addClass('fa fa-comment');
				}

				$.modal.close();

			}, function(error) {
				// error callback
			});

		},

        moment: function () {
            return moment();
        }






	}
});
