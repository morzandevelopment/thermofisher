$(function() {
	$('.switch').on('click', function(){
		$(this).toggleClass('active');
		if ($(this).children('span').text() == 'si')
			$(this).children('span').text('no')
				.next('input[type="checkbox"]').prop('checked', false);
		else
			$(this).children('span').text('si')
				.next('input[type="checkbox"]').prop('checked', true);
	});	
});
