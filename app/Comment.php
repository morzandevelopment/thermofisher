<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model {

  protected $fillable = array('id', 'comment', 'subscriber_id');

  public function subscriber()
  {
      return $this->belongsTo('App\Subscriber');
  }
	//

}
