<?php
/**
 * Created by IntelliJ IDEA.
 * Date: 19/03/17
 * Time: 01:34 PM
 */

namespace App\Http\Controllers;
use App\Filter;
use App\Area;
use Illuminate\Http\Request;
use App\Subscriber;
use Carbon\Carbon;
// use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Input;
use PHPExcel_Worksheet_Drawing;
use Maatwebsite\Excel\Facades\Excel;
use Chumper\Zipper\Zipper;

class AdminController extends Controller {

	public function __construct()
	{
		$this->middleware('auth');
	}

	public function getFilters()
    {
        return $filters = Filter::where('active', 1)->get();
    }

    public function saveFilter(Request $request)
    {
        $filter = Filter::where('id', $request->filterID)->get();
        dd($filter);
        $filter->subscriber()->attach(['filter_id' => $request->filterID, 'subscriber_id' =>$request->subcriberID ]);
    }

	public function index(Subscriber $subscriber)
	{
		$sort_by = Input::get('sortBy');
		$direction = Input::get('direction');
		$rows_by_page = Input::get('rowsByPage');
		$type = Input::get('type');
        $notify_limit_date = $this->compareDates();

        $areas = Area::where('active', 1)->get();
		if(empty($rows_by_page))
		    $rows_by_page = 20;

		$subscribers = $subscriber->getPaginated([
				'sortBy' => $sort_by,
				'direction' => $direction,
				'rowsByPage' => $rows_by_page,
				'type' => $type,
                'filters' => $this->getFilters()
			]);

		$total = $subscriber::count();
		
		return view('admin.index')->with([
			'subscribers' => $subscribers,
			'total' => $total,
			'show' => $rows_by_page,
			'type' => $type,
            'filters' => $this->getFilters(),
            'areas' => $areas,
            'notify_limit_date' => $notify_limit_date
		]);
	}

    public function compareDates() {
        if (!env('RED_LIMIT_DATE')) {
            dd('no hay fecha límite definida en .env');
        }
        else{
            $red_limit_date = Carbon::createFromFormat('d-m-Y', env('RED_LIMIT_DATE'));
            $today = Carbon::today();

            if ($today >= $red_limit_date->subDays(10)) {
                $response = [
                    "response" => true,
                    "date" => env('RED_LIMIT_DATE')
                ];
                return $response;
            }
            else {
                $response = [
                    "response" => false,
                    "date" => env('RED_LIMIT_DATE')
                ];
                return $response;
            }
        }
    }

	public function setCandidate($request){

		$subscribe = Subscriber::find($request);
		if($subscribe->candidate == 1)
		{
			$subscribe->candidate = false;
		}
		else
		{
			$subscribe->candidate = true;
		}
		$subscribe->save();


		return  response()->json(['success' => $subscribe]);

	}

	public function exportToExcel(Request $request)
    {
        $applicants = null;
        $filename = '';
        if ($request->type != 'all')
        {
            $area = Area::where(['slug'=>$request->type])->get()->first();
            $filename = $area->name;
            $applicants = Subscriber::where('type', $request->type)->get();
            $title = 'Listado de postulados para '.$filename.'. Total ( '.count($applicants).' )';
        }
        else
        {
            $applicants = Subscriber::all();
            $title = 'Listado general de todos los postulados. Total ( '.count($applicants).' )';
        }

		$allApplicant = [];

		foreach ($applicants as $applicant)
		{
		    $lada = lada($applicant->phoneAreas_id);
            $array = [
                '#' => $applicant->id,
                'Fecha' => $applicant->created_at->format('d-M-y'),
                'Nombre completo' => $applicant->full_name,
                'Teléfono' => (!empty($applicant->cell_phone) ? $applicant->cell_phone : 'Sin especificar'),
                'Correo electrónico' => (!empty($applicant->email) ? $applicant->email : 'Sin especificar'),
                'Area de interés' => (!empty($applicant->type) ? $applicant->type : 'Sin especificar'),
                'Nivel de inglés' => ( !empty($applicant->english_level) ? $applicant->english_level.'%': 'Sin especificar' ),
                'Candidato' => ( $applicant->candidate == '1') ? 'Sí' : 'No',
                'Comentarios' => ( $applicant->comment) ? $applicant->comment->comment : 'Sin comentarios',
                'CV' => (($applicant->file) ? '=HYPERLINK("http://red.marcavisual.com/thermofisher/public/uploads/cvs/subscribers_'.$applicant->id.'/'.str_replace(' ','%20', $applicant->file->fileurl).'", "Descargar CV")' : 'Sin CV')
            ];
			array_push($allApplicant, $array);
		}

        $alphabet = range('A', 'Z');

        $endCell = $alphabet[count($allApplicant[0])-1];

        $this->makeExcelReport($filename, $allApplicant, $title, $endCell);

	}

    public function makeExcelReport($filename, $allApplicant, $title, $endCell)
    {
        Excel::create('Postulados Rockwell Collins '.$filename, function($excel) use($allApplicant, $title, $endCell) {

            $excel->sheet('Postulados', function($sheet) use ($allApplicant, $title, $endCell){

                $title  = $title;
                $mainTitle = 'Postulados a la campaña';

                $objDrawing = new PHPExcel_Worksheet_Drawing;
                $objDrawing->setPath(public_path('images/header-logo.png'));
                $objDrawing->setCoordinates('A1');
                $objDrawing->setOffsetX(90);
                $objDrawing->setOffsetY(45);
                $objDrawing->setWorksheet($sheet);

                $sheet->fromArray($allApplicant, null, 'A3', true, true);
                $sheet->row(1, array(
                    $mainTitle
                ));
                $sheet->row(2, array(
                    $title
                ));
                $sheet->setHeight(1, 100);
                $sheet->setHeight(2, 35);
                $sheet->setHeight(3, 25);
                $sheet->mergeCells('A1:'.$endCell.'1');
                $sheet->mergeCells('A2:'.$endCell.'2');
                $sheet->setOrientation('landscape');
                $sheet->setPageMargin(0.25);
                $sheet->setFontFamily('Calibri');
                $sheet->setFontSize(12);
                $sheet->setBorder('A3:'.$endCell.'3', 'thin');

                $sheet->cells('A2:'.$endCell.'2', function($cells) {

                    $cells->setFont(array(
                        'family'     => 'Calibri',
                        'size'       => '16',
                        'bold'       =>  true
                    ));
                    $cells->setAlignment('center');
                    $cells->setValignment('center');
                    $cells->setBackground('#424b52');
                    $cells->setFontColor('#ffffff');
                    $cells->setAlignment('center');

                });

                $sheet->cells('A3:'.$endCell.'3', function($cells) {

                    $cells->setFont(array(
                        'family'     => 'Calibri',
                        'size'       => '14',
                        'bold'       =>  true
                    ));

                    $cells->setBackground('#9dadbf');
                    $cells->setFontColor('#ffffff');
                    $cells->setAlignment('center');
                    $cells->setValignment('center');

                });

                $sheet->cells('A1:'.$endCell.'1', function($cells) {

                    $cells->setFont(array(
                        'family'     => 'Calibri',
                        'size'       => '20',
                        'bold'       =>  true
                    ));

                    $cells->setAlignment('center');
                    $cells->setValignment('center');

                });

            });

        })->export('xls');
    }

    public function downloadAll(Request $request) {
        $applicants = Subscriber::all();
        $title = 'Listado general de postulados y cvs Total ( '.count($applicants).' )';
        $allApplicant = [];
        foreach ($applicants as $applicant)
        {
            $lada = lada($applicant->phoneAreas_id);
            $array = [
                '#' => $applicant->id,
                'Fecha' => $applicant->created_at->format('d-M-y'),
                'Nombre completo' => $applicant->full_name,
                'Teléfono' => (!empty($applicant->cell_phone) ? $applicant->cell_phone : 'Sin especificar'),
                'Correo electrónico' => (!empty($applicant->email) ? $applicant->email : 'Sin especificar'),
                'Area de interés' => (!empty($applicant->type) ? $applicant->type : 'Sin especificar'),
                'Nivel de inglés' => ( !empty($applicant->english_level) ? $applicant->english_level.'%': 'Sin especificar' ),
                'Candidato' => ( $applicant->candidate == '1') ? 'Sí' : 'No',
                'Comentarios' => ( $applicant->comment) ? $applicant->comment->comment : 'Sin comentarios',
                // 'CV' => (($applicant->file) ? '=HYPERLINK("http://red.marcavisual.com/rockwell-collins/public/uploads/cvs/subscribers_'.$applicant->id.'/'.str_replace(' ','%20', $applicant->file->fileurl).'", "Descargar CV")' : 'Sin CV')
            ];
            array_push($allApplicant, $array);
        }

        $alphabet = range('A', 'Z');

        $endCell = $alphabet[count($allApplicant[0])-1];

        Excel::create($title, function($excel) use($allApplicant, $title, $endCell) {

            $excel->sheet('Postulados', function($sheet) use ($allApplicant, $title, $endCell){

                $title  = $title;
                $mainTitle = 'Postulados a la campaña';

                $objDrawing = new PHPExcel_Worksheet_Drawing;
                $objDrawing->setPath(public_path('images/header-logo.png'));
                $objDrawing->setCoordinates('B1');
                // $objDrawing->setOffsetX(90);
                $objDrawing->setOffsetY(45);
                $objDrawing->setWorksheet($sheet);

                $sheet->fromArray($allApplicant, null, 'A3', true, true);
                $sheet->row(1, array(
                    $mainTitle
                ));
                $sheet->row(2, array(
                    $title
                ));
                $sheet->setHeight(1, 100);
                $sheet->setHeight(2, 35);
                $sheet->setHeight(3, 25);
                $sheet->mergeCells('A1:'.$endCell.'1');
                $sheet->mergeCells('A2:'.$endCell.'2');
                $sheet->setOrientation('landscape');
                $sheet->setPageMargin(0.25);
                $sheet->setFontFamily('Calibri');
                $sheet->setFontSize(12);
                $sheet->setBorder('A3:'.$endCell.'3', 'thin');

                $sheet->cells('A2:'.$endCell.'2', function($cells) {

                    $cells->setFont(array(
                        'family'     => 'Calibri',
                        'size'       => '16',
                        'bold'       =>  true
                    ));
                    $cells->setAlignment('center');
                    $cells->setValignment('center');
                    $cells->setBackground('#424b52');
                    $cells->setFontColor('#ffffff');
                    $cells->setAlignment('center');

                });

                $sheet->cells('A3:'.$endCell.'3', function($cells) {

                    $cells->setFont(array(
                        'family'     => 'Calibri',
                        'size'       => '14',
                        'bold'       =>  true
                    ));

                    $cells->setBackground('#9dadbf');
                    $cells->setFontColor('#ffffff');
                    $cells->setAlignment('center');
                    $cells->setValignment('center');

                });

                $sheet->cells('A1:'.$endCell.'1', function($cells) {

                    $cells->setFont(array(
                        'family'     => 'Calibri',
                        'size'       => '20',
                        'bold'       =>  true
                    ));

                    $cells->setAlignment('center');
                    $cells->setValignment('center');

                });

            });

        })->store('xls', public_path().'/all_applicant_excell');

        $zip = new Zipper;
        $zip->make('final-zip/cvs.zip');
        $zip->add(public_path().'/uploads/');
        $zip->add(public_path().'/all_applicant_excell');
        $zip->close();

        return response()->download(public_path().'/final-zip/cvs.zip');
    }
}