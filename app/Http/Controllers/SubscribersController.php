<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use App\Http\Requests\SubscriberRequest;
use App\Subscriber;
use App\File;
use App\Lada;
use Illuminate\Support\Facades\Validator;

class SubscribersController extends Controller {

    public function __construct(Lada $lada)
    {
        $this->lada = $lada;

    }

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	public function validateEmail(Request $request) {
		if ($request->input('email') !== '') {
			if ($request->input('email')) {
				$rule = array('email' => 'required|email|unique:subscribers');
				$validator = Validator::make($request->all(), $rule);
			}
			
			if (!$validator->fails()) {
				die('true');
			}
		}

		die('false');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(SubscriberRequest $request)
	{
	    //dd($request->all());
        try {
            if ($request->cell_phone)
            {
                $getLada = $this->get_string_between($request->cell_phone, '(', ')');

                $lada = $this->lada->where('lada', $getLada)->first();
                if ($lada)
                {
                    $request->merge(['phoneAreas_id' => $lada->id]);
                }
            }

        $subscriber =  Subscriber::create($request->all() + (['type' =>'ingeniero-en-aseguramiento-de-calidad']));


      if($request->file('cv')){
        $file = $request->file('cv');
        $fileName = $file->getClientOriginalName();

       $file->move("uploads/cvs/subscribers_$subscriber->id/", $fileName);


       $cvs =  File::Create(['subscriber_id'=>$subscriber->id,'fileurl'=>$fileName ]);

      }

			return response()->json([
				'success' => true,
				'message' => 'record saved'
			], 200);
		} catch (Exception $e) {
			Log::error($e);

			return response()->json([
				'success' => false,
				'message' => $e
			], 422);
		}
	}



  private function uploadImages($request, $input,  $id, $update = false)
  {

    if ($update == true)
    {
      $imagen = Image::find($id);
      $file = $request->file($input);
      $pictureName = $file->getClientOriginalName();

      File::Delete('uploads/companies/'.$imagen->company_id.'/'.$imagen->imageurl);
      $file->move('uploads/companies/'.$imagen->company_id, $pictureName);



      $img = Img::make('uploads/companies/'.$imagen->company_id.'/'. $pictureName);
      $img->resize(200, 200);
      $img->save('uploads/companies/'.$imagen->company_id.'/200_'. $pictureName);




      $imagen->imageurl = $pictureName;
      $imagen->save();

    }
    else
    {
      $file = $request->file($input);
      $pictureName = $file->getClientOriginalName();
      $file->move("uploads/companies/".$id, $pictureName);

      Image::create([
          'imageurl'=>$pictureName,
          'company_id'=>$id,
      ]);
    }


  }

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

    function get_string_between($string, $start, $end)
    {
        $string = ' ' . $string;
        $ini = strpos($string, $start);
        if ($ini == 0) return '';
        $ini += strlen($start);
        $len = strpos($string, $end, $ini) - $ini;
        return substr($string, $ini, $len);
    }

}
