<?php

namespace App\Http\Controllers;
use App\Area;

class WelcomeController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Welcome Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders the "marketing page" for the application and
	| is configured to only allow guests. Like most of the other sample
	| controllers, you are free to modify or remove it as you desire.
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('guest');
	}

	/**
	 * Show the application welcome screen to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
        $areas = Area::where('id','>',0)->where('active', 1)->orderBy('order', 'ASC')->get();
        //$areas = Area::all();
		return view('landing-page', compact('areas'));
	}

	public function dynamicIndex($area)
    {
        $areas = Area::where('id','>',0)->where('active', 1)->orderBy('order', 'ASC')->get();
        if($area == 'ingresar' || $area == 'login')
            return view('auth.login');
        else
            return view('landing-page', compact('area', 'areas'));
    }

    public function specificIndex($area)
    {
        $area = Area::where('slug', $area)->get()->first();
        return view('landing-page-area', compact('area'));
    }

}
