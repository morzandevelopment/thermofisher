<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::get('/manufactura-produccion',function(){
    header("Location: /rockwell-collins/public/gerente-de-manufactura");
die();
});

Route::get('/manufactura',function(){
    header("Location: /rockwell-collins/public/gerente-de-manufactura");
die();
});

Route::get('/produccion',function(){
    header("Location: /rockwell-collins/public/especialista-en-control-de-produccion");
die();
});

// Route::get('/', 'WelcomeController@index');
Route::get('/', ['as' => 'landing.dynamic', 'uses' => 'WelcomeController@index']);
Route::get('/{area}', ['as' => 'landing.dynamic', 'uses' => 'WelcomeController@dynamicIndex']);
//Route::get('/{area}', ['as' => 'landing.dynamic', 'uses' => 'WelcomeController@specificIndex']);

// Route::get('home', 'HomeController@index');

Route::post('subscribe', ['as' => 'subscription.store', 'uses' => 'SubscribersController@store']);
Route::post('validate-email', ['as' => 'validate.email', 'uses' => 'SubscribersController@validateEmail']);

Route::get('/our/terms-and-conditions', function() {
	return view('terms-and-conditions');
});

Route::get('/admin/login', ['as' => 'login', 'uses'	=> 'Auth\AuthController@getLogin']);

// test admin index view

Route::group(['prefix' => 'admin', 'middleware' => ['auth']], function() {

	Route::get('inicio', ['as'=>'admin.index', 'uses'=>'AdminController@index']);
	// Route::get('index/show/', ['as'=>'admin.index', 'uses'=>'AdminController@index']);
	// Route::get('profesionist/show/', ['as'=>'admin.profesionist', 'uses'=>'AdminController@profesionist']);
	// Route::get('production/show/', ['as'=>'admin.production', 'uses'=>'AdminController@production']);
	// Route::get('tecnic/show/', ['as'=>'admin.tecnic', 'uses'=>'AdminController@tecnic']);

  Route::post('export-candidates', ['as'=>'export-excel','uses'=>'AdminController@exportToExcel']);
  Route::resource('comments', 'CommentsController', ['only' => ['store', 'show', 'update']]);
  Route::get('get-filters', 'AdminController@getFilters');
  Route::post('save-filter', 'AdminController@saveFilter');
  Route::post('download-all', ['as'=>'download-all','uses'=>'AdminController@downloadAll']);

});

Route::get('/admin/ingresar', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('/admin/salir', 'Auth\AuthController@getLogout');



Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);

Route::get('setcandidate/{id?}', 'AdminController@setCandidate');

Route::get('/subscribe/get-content-end-applied',function(){
    return view ('end-applied');
});


