<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Input;
use App\Lada;

class Subscriber extends Model {

    protected $fillable = ['full_name', 'cell_phone', 'email', 'candidate', 'type', 'company', 'phoneAreas_id', 'english_level', 'years_experience', 'experience_coordinate_maintenance_plant'];

	public function setEmailAttribute($value) {
		if (empty($value)) {
			$this->attributes['email'] = NULL;
		} else {
			$this->attributes['email'] = $value;
		}
	}

	public function file()
	{
	  return $this->hasOne('App\File');
	}

	public function comment()
	{
	  return $this->hasOne('App\Comment');
	}

	public function filter()
    {
        return $this->belongsToMany('App\Filter', 'filter_subscriber')->withPivot('filter_id', 'subscriber_id');
    }

	public function getPaginated(array $params)
	{	
		if ($this->isSortable($params)) {
			if (isset($params['type']) && $params['type'] != 'all')
				if ($params['sortBy'] == 'cv') {
					$params['sortBy'] = 'subscriber_id';
					return $this->leftJoin('files', 'files.subscriber_id', '=', 'subscribers.id')->select('subscribers.*')->where('type', $params['type'])->orderBy($params['sortBy'], $params['direction'])->paginate($params['rowsByPage']);
				} else {
					return $this->where('type', $params['type'])->orderBy($params['sortBy'], $params['direction'])->paginate($params['rowsByPage']);
				}
			else if (!isset($params['type']) || $params['type'] == 'all')
				if ($params['sortBy'] == 'cv') {
					$params['sortBy'] = 'subscriber_id';
					return $this->leftJoin('files', 'files.subscriber_id', '=', 'subscribers.id')->select('subscribers.*')->orderBy($params['sortBy'], $params['direction'])->paginate($params['rowsByPage']);
				} else {
					return $this->orderBy($params['sortBy'], $params['direction'])->paginate($params['rowsByPage']);
				}
		} else {
			if (isset($params['type']) && $params['type'] != 'all')
				return $this->where('type', $params['type'])->paginate($params['rowsByPage']);
			else if (!isset($params['type']) || $params['type'] == 'all')
				return $this->paginate($params['rowsByPage']);
		}

		return $this->paginate($params['rowsByPage']);
	}

	protected function isSortable(array $params)
	{
		return $params['sortBy'] and $params['direction'] and $params['rowsByPage'] and $params['type'];
	}
}
