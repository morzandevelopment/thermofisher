<?php

use Illuminate\Support\Facades\Input;
use App\Lada;

function sortSubscribersBy($column)
{
    $direction = (Input::get('direction') == 'desc') ? 'asc' : 'desc';
    
    $rows_by_page = Input::get('rowsByPage');
    if(!isset($rows_by_page))
		$rows_by_page = 20;

    $type = Input::get('type');
    if(!isset($type))
		$type = 'all';

    return route('admin.index', [
        'sortBy' => $column,
        'direction' => $direction,
        'rowsByPage' => $rows_by_page,
        'type' => $type
    ]);
}

function lada($lada)
{
    return Lada::where('id', $lada)->get();
}
