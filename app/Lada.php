<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Lada extends Model {

    protected $table = 'phoneAreas';
    protected $fillable = [
        'id',
        'location',
        'lada'
    ];

    public function subscriber()
    {
        return $this->belongsTo('App\Subscriber', 'phoneAreas_id', 'id');
    }

}
